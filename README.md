# lib-nebulous

A UI agnostic implementation of Nebulous's core functionality. If you want to make your own version of Nebulous, you can just add this as a dependency (there are no stability guarantees).