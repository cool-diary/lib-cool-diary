/*!
lib-nebulous is a platform/UI independent implementation of cool-diary's core logic.

This being seperate from the UI allows separation of concerns & experimenting either on this or on the GUI independently
*/

#![warn(missing_docs)]
#![warn(clippy::cargo)]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![allow(clippy::must_use_candidate)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::cast_lossless)]
#![allow(clippy::redundant_pub_crate)]
#![allow(clippy::use_self)]
#![allow(clippy::explicit_auto_deref)] // Wrong more often than not
#![allow(clippy::implicit_clone)]
#![allow(clippy::single_match_else)]

mod categories;
mod config_manager;
mod data_state;
mod data_versions;
mod encryption;
mod filters;
mod meta;
mod meta_secure;
mod sections;
mod time;
mod transformer;

use std::time::{SystemTime, UNIX_EPOCH};

pub use categories::*;
pub use config_manager::*;
pub use data_state::*;
pub use encryption::*;
pub use filters::*;
pub use meta::*;
pub use meta_secure::*;
pub use sections::*;
pub use time::*;
pub use transformer::*;

/// Get the current time
///
/// # Panics
/// Will panic if the system time is before the UNIX epoch.
pub fn now() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("System time shouldn't be before the UNIX epoch")
        .as_secs()
}
