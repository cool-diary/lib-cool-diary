use std::cmp::Ordering;

use error_stack::{Report, Result, ResultExt};
use error_stack_error_macro::error;
use log::debug;

use crate::TryRepair;

/// Represents a date
#[derive(Clone, Copy, PartialEq, Eq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Date {
    pub(crate) year: i32,
    pub(crate) month: u8,
    pub(crate) day: u8,
}

impl Date {}

error!(pub DateRepairError; "Failed to validate or repair Date");

impl TryRepair for Date {
    type Error = DateRepairError;

    fn try_repair(&mut self) -> Result<(), Self::Error> {
        if self.month > 11 {
            return Err(Report::new(DateRepairError).attach_printable("Month out of range"));
        }

        if self.day < 1 || self.day > 31 {
            return Err(Report::new(DateRepairError).attach_printable("Day out of range"));
        }

        Ok(())
    }
}

impl PartialOrd for Date {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Date {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.year.cmp(&other.year) {
            Ordering::Equal => match self.month.cmp(&other.month) {
                Ordering::Equal => self.day.cmp(&other.day),
                ord => ord,
            },
            ord => ord,
        }
    }
}

/// Represents a time span between two dates
#[derive(Clone, Copy, PartialEq, Eq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Range {
    start: Date,
    end: Date,
}

impl Range {
    /// Create a new range
    ///
    /// Will flip the start and end dates if they're out of order
    pub fn new(start: Date, end: Date) -> Range {
        let range = Range { start, end };

        range.fix_order()
    }

    fn fix_order(self) -> Self {
        if self.start <= self.end {
            self
        } else {
            debug!("The start and end of a range were swapped");
            Range {
                start: self.end,
                end: self.start,
            }
        }
    }

    /// The range's start date
    pub const fn start(self) -> Date {
        self.start
    }

    /// The range's end date
    pub const fn end(self) -> Date {
        self.end
    }

    /// Does the range include the given date (inclusive)?
    pub fn includes(self, date: Date) -> bool {
        date >= self.start && date <= self.end
    }

    /// Does the range overlap with the other range (inclusive)?
    pub fn intersects_with(self, other: Range) -> bool {
        self.includes(other.start)
            || self.includes(other.end)
            || other.includes(self.start)
            || other.includes(self.end)
    }
}

error!(pub RangeRepairError; "Failed to repair and validate a Range");

impl TryRepair for Range {
    type Error = RangeRepairError;

    fn try_repair(&mut self) -> Result<(), Self::Error> {
        self.start.try_repair().change_context(RangeRepairError)?;

        self.end.try_repair().change_context(RangeRepairError)?;

        *self = self.fix_order();

        Ok(())
    }
}

impl From<(Date, Date)> for Range {
    fn from(v: (Date, Date)) -> Self {
        Range {
            start: v.0,
            end: v.1,
        }
        .fix_order()
    }
}

impl From<Range> for (Date, Date) {
    fn from(range: Range) -> Self {
        let range_fixed = range.fix_order();

        (range_fixed.start, range_fixed.end)
    }
}

/// The time options that can be associated with a section or filtered by
#[derive(Clone, Copy, PartialEq, Eq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub enum Time {
    /// Not associated with a time
    None,
    /// Associated with a date
    Date(Date),
    /// Associated with a range of time
    Range(Range),
}

error!(pub TimeRepairError; "Failed to repair or validate Time");

impl TryRepair for Time {
    type Error = TimeRepairError;

    fn try_repair(&mut self) -> Result<(), Self::Error> {
        match self {
            Time::None => Ok(()),
            Time::Date(date) => date.try_repair().change_context(TimeRepairError),
            Time::Range(range) => range.try_repair().change_context(TimeRepairError),
        }
    }
}

impl Time {
    /// Check whether two times overlap
    pub fn overlaps(self, other: Time) -> bool {
        use Time::{Date, None, Range};

        match (self, other) {
            (None, None) => true,
            (None, _) | (_, None) => false,
            (Date(date1), Date(date2)) => date1 == date2,
            (Date(date), Range(range)) | (Range(range), Date(date)) => range.includes(date),
            (Range(range1), Range(range2)) => range1.intersects_with(range2),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::transformer::TryRepair;
    use std::cmp::Ordering;
    use Ordering::{Equal, Greater, Less};

    use crate::{Date, Range, Time};
    use test_case::test_case;

    #[test_case(Time::None, true ; "None is valid")]
    #[test_case(Time::Date(Date {year: 0, month: 0, day: 0}), false  ; "Day invalid")]
    #[test_case(Time::Date(Date {year: 0, month: 12, day: 1}), false ; "Month invalid")]
    #[test_case(Time::Date(Date {year: 0, month: 11, day: 1}), true  ; "Date valid")]
    #[test_case(Time::Range(Range { start: Date { year: 0, month: 11, day: 1 }, end: Date { year: 0, month: 10, day: 12 } }), true  ; "Range backwards (it should fix it)")]
    #[test_case(Time::Range(Range { start: Date { year: 0, month: 9, day: 1 }, end: Date { year: 0, month: 10, day: 12 } }), true   ; "Range valid")]
    fn option_valid(mut option: Time, is_valid: bool) {
        assert_eq!(option.try_repair().is_ok(), is_valid);
    }

    #[test_case(Date { year: 0, month: 2, day: 2 }, Equal   ; "Equal")]
    #[test_case(Date { year: 0, month: 2, day: 1 }, Greater ; "Day greater")]
    #[test_case(Date { year: 0, month: 1, day: 3 }, Greater ; "Month greater")]
    #[test_case(Date { year:-1, month: 3, day: 3 }, Greater ; "Year greater")]
    #[test_case(Date { year: 0, month: 2, day: 3 }, Less    ; "Day less")]
    #[test_case(Date { year: 0, month: 3, day: 1 }, Less    ; "Month less")]
    #[test_case(Date { year: 1, month: 1, day: 1 }, Less    ; "Year less")]
    fn date_comparison(rhs: Date, expected_result: Ordering) {
        assert_eq!(
            Date {
                year: 0,
                month: 2,
                day: 2
            }
            .cmp(&rhs),
            expected_result
        );
    }

    #[test_case(Date { year: 0, month: 1, day: 1 }, false ; "Less")]
    #[test_case(Date { year: 3, month: 1, day: 1 }, false ; "Greater")]
    #[test_case(Date { year: 1, month: 1, day: 1 }, true  ; "Lower bound")]
    #[test_case(Date { year: 2, month: 1, day: 1 }, true  ; "Upper bound")]
    #[test_case(Date { year: 1, month: 2, day: 1 }, true  ; "Inside")]
    fn range_includes(date: Date, expected_result: bool) {
        assert_eq!(
            Range {
                start: Date {
                    year: 1,
                    month: 1,
                    day: 1,
                },
                end: Date {
                    year: 2,
                    month: 1,
                    day: 1,
                }
            }
            .includes(date),
            expected_result
        );
    }

    #[test_case((Date { year: 0, month: 1, day: 1 }, Date { year: 2, month: 2, day: 1 }), true)]
    #[test_case((Date { year: 1, month: 2, day: 1 }, Date { year: 2, month: 2, day: 1 }), true)]
    #[test_case((Date { year: 2, month: 1, day: 2 }, Date { year: 2, month: 2, day: 1 }), false)]
    fn range_intersects(range: (Date, Date), expected_result: bool) {
        assert_eq!(
            Range {
                start: Date {
                    year: 1,
                    month: 1,
                    day: 1,
                },
                end: Date {
                    year: 2,
                    month: 1,
                    day: 1,
                }
            }
            .intersects_with(range.into()),
            expected_result
        );
    }
}
