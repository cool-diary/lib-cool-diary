use std::{collections::HashMap, convert::Infallible, ops::Deref};

use error_stack::Result;

use log::trace;

use crate::TryRepair;

/// A category, can be assigned to sections, be filtered by, and have parents
#[derive(Clone, Eq, PartialEq)]
#[cfg_attr(any(debug_assertions, test), derive(Debug))]
pub struct Category {
    pub(crate) name: String,
    pub(crate) parent: Option<u32>,
    pub(crate) color: String,
    pub(crate) closed: bool,
}

impl Category {
    /// Get the name of the category
    pub fn get_name(&self) -> &str {
        &self.name
    }

    /// Get the category's parent
    pub const fn get_parent(&self) -> Option<&u32> {
        self.parent.as_ref()
    }

    /// Get the category's color
    pub fn get_color(&self) -> &str {
        &self.color
    }

    /// Is the category closed?
    pub const fn is_closed(&self) -> bool {
        self.closed
    }

    /// Set a category's name
    pub fn set_name(&mut self, name: String) {
        self.name = name;
    }

    /// Set a category's parent
    pub fn set_parent(&mut self, parent: Option<u32>) {
        self.parent = parent;
    }

    /// Set a category's color
    pub fn set_color(&mut self, color: String) {
        self.color = color;
    }

    /// Set whether a category is closed or not (it's children collapsed)
    pub fn set_closed(&mut self, closed: bool) {
        self.closed = closed;
    }
}

/// The total set of categories that exist in the diary
///
/// Serialized/deserialized by converting to/from a `Vec<CategoryInTree>`
#[derive(Clone, Eq, PartialEq, Default)]
#[cfg_attr(any(debug_assertions, test), derive(Debug))]
pub struct Categories(pub(crate) HashMap<u32, Category>);

impl TryRepair for Categories {
    type Error = Infallible;

    fn try_repair(&mut self) -> Result<(), Infallible> {
        let mut ids_to_reset_parents = Vec::new();

        for (id, category) in &self.0 {
            if let Some(parent) = category.parent {
                if !self.0.contains_key(&parent) {
                    ids_to_reset_parents.push(*id);
                }
            }
        }

        for id in ids_to_reset_parents {
            if let Some(cat) = self.0.get_mut(&id) {
                cat.parent = None;
            }
        }

        Ok(())
    }
}

impl Categories {
    /// Create a new category
    pub fn create_category(&mut self, name: String) -> (u32, &mut Category) {
        trace!("Creating a new category");

        let id = 1 + self.0.iter().fold(0, |a, (id, _)| a.max(*id));

        (
            id,
            self.0.entry(id).or_insert(Category {
                name,
                parent: None,
                color: "white".to_owned(),
                closed: false,
            }),
        )
    }

    /// Get a category by its id
    pub fn get_category(&self, id: u32) -> Option<&Category> {
        self.0.get(&id)
    }

    /// Change a category by its id
    pub fn get_category_mut(&mut self, id: u32) -> Option<&mut Category> {
        self.0.get_mut(&id)
    }

    /// Takes in a list of categories, and appends their parents, and their parents, and etc.
    pub fn add_parents(&self, mut ids: Vec<u32>) -> Vec<u32> {
        let mut i = 0;

        while i < ids.len() {
            if let Some(parent) = ids.get(i).and_then(|id| self.get_category(*id)?.parent) {
                if !ids.contains(&parent) {
                    ids.push(parent);
                }
            }

            i += 1;
        }

        ids
    }

    pub(crate) fn delete_category(&mut self, category_id: u32) {
        trace!("Deleting a category");

        let parent = self.0.get(&category_id).and_then(|v| v.parent);

        self.0.remove(&category_id);

        for category in self.0.values_mut() {
            if category.parent == Some(category_id) {
                category.parent = parent;
            }
        }
    }

    /// Takes in a list of categories, and removes all of the categories that are ancestors of the other categories in the list
    pub fn remove_parents(&self, category_ids: &mut Vec<u32>) {
        let mut removed = true;

        // Repeat removing parents until there are none yet (in some cases, parents could be before children)
        while removed {
            removed = false;

            let mut i = 0;

            while i < category_ids.len() {
                removed |= self.remove_parents_of(category_ids[i], category_ids);

                i += 1;
            }
        }
    }

    /// Remove all the parents of a category in a list and return whether any categories were removed
    pub(crate) fn remove_parents_of(&self, category: u32, ids: &mut Vec<u32>) -> bool {
        if let Some(parent) = self.get_category(category).and_then(|cat| cat.parent) {
            let len_prev = ids.len();

            ids.retain(|id| *id != parent);

            return self.remove_parents_of(parent, ids) || ids.len() < len_prev;
        }

        false
    }

    pub(crate) fn remove_subcategories_of(&self, category_id: u32, ids: &mut Vec<u32>) {
        let mut to_remove = vec![];

        let mut i = 0;
        while i < ids.len() {
            println!("{:?}", self.get_category(ids[i]).and_then(|v| v.parent));
            if self
                .get_category(ids[i])
                .map_or(false, |v| v.parent == Some(category_id))
            {
                to_remove.push(ids.remove(i));
            } else {
                i += 1;
            }
        }

        for subcategory in to_remove {
            self.remove_subcategories_of(subcategory, ids);
        }
    }

    /// The set of category ids
    pub fn ids(&self) -> impl Iterator<Item = &u32> {
        self.0.keys()
    }
}

/// Contains categories which can be selected
#[derive(Clone)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[repr(transparent)]
pub struct CategorySelector(Vec<u32>);

impl Deref for CategorySelector {
    type Target = [u32];

    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}

impl CategorySelector {
    /// Create a new `CategorySelector` from a list of ids
    pub const fn new() -> Self {
        CategorySelector(vec![])
    }

    pub(crate) fn into_inner(self) -> Vec<u32> {
        self.0
    }

    pub(crate) fn new_with(categories: Vec<u32>) -> Self {
        CategorySelector(categories)
    }

    /// Remove a category from the section
    pub fn remove_category(&mut self, category_id: u32) {
        self.0.retain(|v| *v != category_id);
    }

    /// Add a category to the section
    pub fn add_category(&mut self, category_id: u32, categories: &Categories) {
        if self.0.contains(&category_id) || categories.get_category(category_id).is_none() {
            return;
        }

        self.0.push(category_id);

        categories.remove_parents_of(category_id, &mut self.0);
        categories.remove_subcategories_of(category_id, &mut self.0);
    }

    /// Repair the data inside if it's messed up
    pub fn repair(&mut self, categories: &Categories) {
        self.0.retain(|v| categories.get_category(*v).is_some());

        categories.remove_parents(&mut self.0);
    }
}

#[cfg(test)]
mod tests {
    use crate::Categories;
    use test_case::test_case;

    fn dummy_categories() -> Categories {
        let mut categories = Categories::default();

        let (id0, _cat0) = categories.create_category(String::new());

        let (id1, cat1) = categories.create_category(String::new());
        cat1.set_parent(Some(id0));

        let (_, cat2) = categories.create_category(String::new());
        cat2.set_parent(Some(id1));

        let (_, cat3) = categories.create_category(String::new());
        cat3.set_parent(Some(id1));

        categories
    }

    #[test_case(1, true)]
    #[test_case(2, true)]
    #[test_case(5, false)]
    fn get_categories(id: u32, exists: bool) {
        let mut categories = dummy_categories();
        assert_eq!(categories.get_category(id).is_some(), exists);
        assert_eq!(categories.get_category_mut(id).is_some(), exists);
    }

    #[test]
    fn delete_category() {
        let mut categories = dummy_categories();
        categories.delete_category(2);

        assert_eq!(categories.get_category(3).unwrap().parent, Some(1));
        assert_eq!(categories.get_category(4).unwrap().parent, Some(1));
    }

    #[test_case(vec! [3], &[3, 2, 1])]
    #[test_case(vec! [2], &[2, 1])]
    #[test_case(vec! [1], &[1])]
    fn add_parents(start: Vec<u32>, expected: &[u32]) {
        let categories = dummy_categories();

        assert_eq!(categories.add_parents(start), expected);
    }

    #[test_case(vec! [3], &[3])]
    #[test_case(vec! [3, 2], &[3])]
    #[test_case(vec! [3, 1], &[3])]
    #[test_case(vec! [2, 1], &[2])]
    #[test_case(vec! [3, 4, 2], &[3, 4])]
    fn remove_parents(mut start: Vec<u32>, expected: &[u32]) {
        let categories = dummy_categories();
        categories.remove_parents(&mut start);

        assert_eq!(start, expected);
    }

    #[test_case(vec! [2], 2, &[2])]
    #[test_case(vec! [2, 3], 2, &[2])]
    #[test_case(vec! [1, 2, 3], 1, &[1])]
    #[test_case(vec! [1, 2, 3], 2, &[1, 2])]
    fn remove_subcategories(mut start: Vec<u32>, of: u32, expected: &[u32]) {
        let categories = dummy_categories();
        categories.remove_subcategories_of(of, &mut start);

        assert_eq!(start, expected);
    }
}
