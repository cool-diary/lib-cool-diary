use aes_gcm_siv::{
    aead::{Aead, Payload},
    Aes256GcmSiv, KeyInit, Nonce,
};
use bcrypt_pbkdf::bcrypt_pbkdf;
use electra::NiceErrorMessage;
use error_stack::{Report, Result, ResultExt};
use error_stack_error_macro::error;
use log::{debug, trace};
use secrecy::{ExposeSecret, Secret};
use std::{borrow::Cow, fmt::Debug};
use tokio::runtime::Handle;

error!(pub EncryptionFailed; "Encryption failed");
error!(pub DecryptionError; "Failed to decrypt ciphertext");

error!(pub PlaintextZeroSize; "The data being encrypted is empty");
error!(pub IncorrectKey; "The decryption key inputted is incorrect");

pub(crate) fn maybe_encrypt<'a>(
    maybe_key: Option<&Key>,
    nonce_bytes: [u8; 12],
    plaintext: Cow<'a, [u8]>,
) -> Result<Cow<'a, [u8]>, EncryptionFailed> {
    Ok(match maybe_key {
        Some(key) => Cow::from(encrypt(key, nonce_bytes, &plaintext)?),
        None => plaintext,
    })
}

pub(crate) fn maybe_decrypt<'a>(
    maybe_key: Option<&Key>,
    ciphertext: Cow<'a, [u8]>,
) -> Result<Cow<'a, [u8]>, DecryptionError> {
    Ok(match maybe_key {
        Some(key) => Cow::from(decrypt(key, &ciphertext)?),
        None => ciphertext,
    })
}

pub(crate) fn encrypt(
    key: &Key,
    nonce_bytes: [u8; 12],
    plaintext: &[u8],
) -> Result<Vec<u8>, EncryptionFailed> {
    trace!("Encrypting something");

    if plaintext.is_empty() {
        return Err(NiceErrorMessage::attach(
            Report::new(EncryptionFailed),
            PlaintextZeroSize,
        ));
    }

    let key = aes_gcm_siv::Key::<Aes256GcmSiv>::from_slice(key.key.expose_secret());
    let cipher = Aes256GcmSiv::new(key);

    let nonce = Nonce::from_slice(&nonce_bytes);

    let mut ciphertext = cipher
        .encrypt(
            nonce,
            Payload {
                msg: plaintext,
                aad: &nonce_bytes,
            },
        )
        .change_context(EncryptionFailed)?;

    ciphertext.append(&mut Vec::from(nonce_bytes));

    Ok(ciphertext)
}

pub(crate) fn decrypt(key: &Key, ciphertext: &[u8]) -> Result<Vec<u8>, DecryptionError> {
    trace!("Decrypting something");

    let cipher = Aes256GcmSiv::new(aes_gcm_siv::Key::<Aes256GcmSiv>::from_slice(
        key.key.expose_secret(),
    ));

    let len = ciphertext.len();

    if len < 12 {
        return Err(
            Report::new(DecryptionError).attach_printable("Length of ciphertext is too short")
        );
    }

    let encrypted = &ciphertext[0..len - 12];
    let nonce = &ciphertext[len - 12..len];

    NiceErrorMessage::maybe_attach(
        cipher
            .decrypt(
                Nonce::from_slice(nonce),
                Payload {
                    msg: encrypted,
                    aad: nonce,
                },
            )
            .change_context(DecryptionError),
        IncorrectKey,
    )
}

error!(pub GenerateKeyError; "Failed to generate a key");

/// Convert a password to a key & the data required to reproduce the key
///
/// Use this when setting a password
pub async fn encryption_data_from_password(
    handle: &Handle,
    password: Secret<Vec<u8>>,
    rounds: u32,
) -> Result<(Key, HashingData), GenerateKeyError> {
    debug!("Generating encryption data and a key from a password");

    let data = HashingData::new(rand::random(), rounds);

    Ok((
        Key::generate_key(handle, password, data)
            .await
            .attach_printable("Failed to generate key from given password")
            .change_context(GenerateKeyError)?,
        data,
    ))
}

/// The data neccessary to reproduce a decryption key from a password
#[derive(Debug, Clone, Copy)]
pub struct HashingData {
    pub(crate) salt: [u8; 16],
    pub(crate) rounds: u32,
}

impl HashingData {
    pub(crate) const fn new(salt: [u8; 16], rounds: u32) -> HashingData {
        HashingData { salt, rounds }
    }

    /// Reproduce the key from the password
    pub async fn generate_key(
        self,
        handle: &Handle,
        password: Secret<Vec<u8>>,
    ) -> Result<Key, KeyGenerateError> {
        Key::generate_key(handle, password, self).await
    }
}

error!(pub KeyGenerateError; "Failed to generate key");

/// An encryption key
#[derive(Clone)]
pub struct Key {
    key: Secret<[u8; 32]>,
}

impl Key {
    /// Create a `Key` struct using a custom key
    pub const fn custom(key: Secret<[u8; 32]>) -> Key {
        Key { key }
    }

    /// Derive a key from a password using bcrypt-pbkdf
    pub async fn generate_key(
        handle: &tokio::runtime::Handle,
        password: Secret<Vec<u8>>,
        encryption_data: HashingData,
    ) -> Result<Key, KeyGenerateError> {
        handle
            .spawn_blocking(move || {
                debug!("Generating a key from a password and encryption data");

                let mut key = [0; 32];

                bcrypt_pbkdf(
                    password.expose_secret(),
                    &encryption_data.salt,
                    encryption_data.rounds.max(1),
                    &mut key,
                )
                .attach_printable("Arguments to bcrypt_pbkdf are incorrect")
                .change_context(KeyGenerateError)?;

                Ok(Key {
                    key: Secret::new(key),
                })
            })
            .await
            .attach_printable("Failed to join task")
            .change_context(KeyGenerateError)?
    }
}

#[cfg(debug_assertions)]
impl Debug for Key {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Key")
    }
}

#[cfg(test)]
#[allow(clippy::cast_possible_truncation)]
mod tests {
    use crate::{decrypt, encrypt, Key};

    #[test]
    fn encryption() {
        let data = (0..40)
            .enumerate()
            .map(|(i, _)| i as u8)
            .collect::<Vec<u8>>();

        let key = Key::custom(
            TryInto::<[u8; 32]>::try_into(
                (0..32)
                    .enumerate()
                    .map(|(i, _)| i as u8)
                    .collect::<Vec<u8>>(),
            )
            .unwrap()
            .into(),
        );

        let nonce = (0..12)
            .enumerate()
            .map(|(i, _)| i as u8)
            .collect::<Vec<u8>>()
            .try_into()
            .unwrap();

        let mut encrypted = encrypt(&key, nonce, &data).unwrap();

        // Make sure encryption doesn't change
        assert_eq!(
            encrypted,
            [
                220, 24, 94, 43, 127, 175, 223, 165, 185, 219, 17, 73, 63, 17, 91, 63, 221, 144,
                74, 178, 23, 244, 185, 29, 145, 201, 199, 198, 248, 246, 63, 226, 134, 89, 227, 0,
                130, 196, 70, 162, 68, 133, 32, 75, 143, 227, 179, 157, 91, 243, 55, 188, 2, 10,
                248, 97, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
            ]
        );

        // Decryption works
        assert_eq!(data, decrypt(&key, &encrypted).unwrap());

        // Wrong key throws error
        assert!(decrypt(&Key::custom([0; 32].into()), &encrypted).is_err());

        encrypted[8] ^= 1;

        // Throws error if the ciphertext has been changed
        assert!(decrypt(&key, &encrypted).is_err());
    }
}
