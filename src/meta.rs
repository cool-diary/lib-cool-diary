use std::{convert::Infallible, path::Path};

use electra::{
    managable::{Managable, StaticName},
    Reference,
};
use error_stack::Result;
use serde::{Deserialize, Serialize};

use crate::{encryption::HashingData, TryRepair};

/// All the data associated with the diary that needs to be accessed before it can be decrypted
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(
    from = "crate::data_versions::MetaVersions",
    into = "crate::data_versions::MetaVersions"
)]
pub struct Meta {
    encryption: Option<HashingData>,
}

impl Meta {
    /// Create a new `Meta` instance
    pub const fn new(encryption: Option<HashingData>) -> Self {
        Self { encryption }
    }

    /// Get the diary's `HashingData`
    pub const fn encryption(&self) -> Option<HashingData> {
        self.encryption
    }

    /// Set the diary's encryption. It doesn't re-encrypt any data, just changes the metadata.
    pub(crate) fn set_encryption(&mut self, hashing_data: Option<HashingData>) {
        self.encryption = hashing_data;
    }
}

impl TryRepair for Meta {
    type Error = Infallible;

    fn try_repair(&mut self) -> Result<(), Infallible> {
        Ok(())
    }
}

impl Managable<Reference<Path>, Vec<u8>> for Meta {
    type Name = StaticName<Reference<Path>>;

    const STATIC_NAME_INPUT: fn() -> Reference<Path> =
        || Reference::Borrowed("meta.cbor.gz".as_ref());

    type TransformerParams = ();

    type EncodeError = crate::EncodeError;
    type DecodeError = crate::DecodeError;

    fn encode(&mut self, (): ()) -> Result<Vec<u8>, Self::EncodeError> {
        crate::encode(self)
    }

    fn decode(data: Vec<u8>, (): ()) -> Result<Self, Self::DecodeError> {
        crate::decode(&data)
    }
}
