use std::{
    any::type_name,
    fmt::Debug,
    hash::Hash,
    io::Read,
    marker::PhantomData,
    path::{Path, PathBuf},
};

use base64::{engine::general_purpose::URL_SAFE, Engine};
use brotli::{CompressorReader, Decompressor};
use electra::{
    managable::{random_name, GenerateRandomNameError, Managable, NameType},
    Reference, StorageAccess,
};
use error_stack::{Context, Report, Result, ResultExt};
use error_stack_error_macro::error;
use log::trace;
use serde::{de::DeserializeOwned, Serialize};

use crate::{
    decrypt, encrypt,
    encryption::{maybe_decrypt, maybe_encrypt, EncryptionFailed, Key},
};

/// A type that can be validated and repaired
pub trait TryRepair {
    /// The error if repairing and validating fails
    type Error: Context;

    /// Repair and validate the type
    fn try_repair(&mut self) -> Result<(), Self::Error>;
}

error!(pub EncodeError; "Failed to encode a type");

/// Encode a type with CBOR & brotli while calling `try_repair`
pub fn encode<T: Serialize + TryRepair>(v: &mut T) -> Result<Vec<u8>, EncodeError> {
    trace!("Encoding a {}", type_name::<T>());

    v.try_repair().change_context(EncodeError)?;

    let mut encoded = Vec::new();

    ciborium::ser::into_writer(v, &mut encoded).change_context(EncodeError)?;

    compress(&encoded).change_context(EncodeError)
}

error!(pub DecodeError; "Failed to decode a type");

/// Decode a type with CBOR & brotli while calling `try_repair`
pub fn decode<T: DeserializeOwned + TryRepair>(encoded: &[u8]) -> Result<T, DecodeError> {
    let mut v: T = ciborium::de::from_reader(&*decompress(encoded).change_context(DecodeError)?)
        .change_context(DecodeError)?;

    v.try_repair().change_context(DecodeError)?;

    Ok(v)
}

error!(pub MaybeEncryptError; "Failed to maybe encrypt a type");

/// Encode a type with CBOR & brotli with encryption while calling `try_repair`
pub fn encode_with_encryption<T: Serialize + TryRepair>(
    v: &mut T,
    maybe_key: &Option<Key>,
) -> Result<Vec<u8>, MaybeEncryptError> {
    let encoded = encode(v).change_context(MaybeEncryptError)?;

    trace!("Maybe encrypting a {}", type_name::<T>());

    let nonce = rand::random();

    Ok(maybe_encrypt(maybe_key.as_ref(), nonce, encoded.into())
        .change_context(MaybeEncryptError)?
        .into_owned())
}

error!(pub MaybeDecryptError; "Failed to maybe decrypt a type");

/// Decode a type with CBOR & brotli with encryption while calling `try_repair`
pub fn decode_with_decryption<T: DeserializeOwned + TryRepair>(
    encrypted: &[u8],
    maybe_key: &Option<Key>,
) -> Result<T, MaybeDecryptError> {
    let decrypted =
        maybe_decrypt(maybe_key.as_ref(), encrypted.into()).change_context(MaybeDecryptError)?;

    decode(&decrypted).change_context(MaybeDecryptError)
}

/// A type that encodes a randomly generated file name
pub struct RandomFileKey<M>(pub(crate) [u8; 32], PhantomData<M>);

#[cfg(debug_assertions)]
impl<M> Debug for RandomFileKey<M> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<M> Eq for RandomFileKey<M> {}

impl<M> PartialEq for RandomFileKey<M> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<M> Hash for RandomFileKey<M> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<M> Clone for RandomFileKey<M> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<M> Copy for RandomFileKey<M> {}

impl<M> RandomFileKey<M> {
    /// Generate a new and unique `RandomFileKey`; it's theoretically possible for it to generate the same key twice if the previous one hasn't been written, but that's super unlikely
    pub async fn generate<SA: StorageAccess<FileKey = Reference<Path>> + ?Sized>(
        sa: &SA,
        key: Option<Key>,
    ) -> Result<RandomFileKey<M>, GenerateRandomNameError>
    where
        M: Managable<SA::FileKey, SA::DataType, Name = RandomFileKeyName>,
    {
        random_name::<SA, M>(sa, key, || RandomFileKey::<M>(rand::random(), PhantomData)).await
    }

    pub(crate) const fn from_data(data: [u8; 32]) -> RandomFileKey<M> {
        RandomFileKey(data, PhantomData)
    }
}

/// Indicates the file name of a type must come from a `RandomFileKey`
pub struct RandomFileKeyName;

error!(pub RandomFileKeyDecodingError; "Failed to decode name as an encoded RandomFileKey");

impl<
        DataType: Send + Sync + 'static,
        M: Managable<Reference<Path>, DataType, Name = RandomFileKeyName>,
    > NameType<Reference<Path>, DataType, M> for RandomFileKeyName
{
    type Input = RandomFileKey<M>;

    type StaticInput = &'static [u8; 8];

    type TransformerParams = Option<Key>;

    type EncodeError = EncryptionFailed;
    fn encode(
        input: &RandomFileKey<M>,
        maybe_key: Option<Key>,
    ) -> Result<Reference<Path>, Self::EncodeError> {
        trace!("Encoding a RandomFileKey");

        let mut value = [0; 40];
        let (a, b) = value.split_at_mut(32);

        a.copy_from_slice(&input.0);
        b.copy_from_slice(M::STATIC_NAME_INPUT);

        let maybe_encrypted = match maybe_key.as_ref() {
            Some(key) => encrypt(
                key,
                <&[u8] as TryInto<[u8; 12]>>::try_into(&value[0..12])
                    .change_context(EncryptionFailed)?,
                &value[12..],
            )?,
            None => value.into(),
        };

        let encoded = URL_SAFE.encode(maybe_encrypted);

        Ok(Reference::Arc(PathBuf::from(encoded).into()))
    }

    type DecodeError = RandomFileKeyDecodingError;
    fn decode(
        path: &Reference<Path>,
        maybe_key: Option<Key>,
    ) -> Result<RandomFileKey<M>, Self::DecodeError> {
        trace!("Decoding a RandomFileKey");

        let decoded = URL_SAFE
            .decode(
                path.file_name()
                    .ok_or_else(|| {
                        Report::new(RandomFileKeyDecodingError)
                            .attach_printable("The path given doesn't include a file name")
                    })?
                    .to_str()
                    .ok_or_else(|| {
                        Report::new(RandomFileKeyDecodingError)
                            .attach_printable("The file name isn't a valid string")
                    })?,
            )
            .change_context(RandomFileKeyDecodingError)?;

        let decrypted = match maybe_key.as_ref() {
            Some(key) => {
                let mut ret = decoded[(decoded.len() - 12)..(decoded.len())].to_vec();

                let mut decrypted =
                    decrypt(key, &decoded).change_context(RandomFileKeyDecodingError)?;

                ret.append(&mut decrypted);

                ret
            }
            None => decoded,
        };

        if &decrypted[32..] != M::STATIC_NAME_INPUT {
            return Err(Report::new(RandomFileKeyDecodingError)
                .attach_printable("File name indicates that it contains the wrong type of data"));
        }

        Ok(RandomFileKey(
            decrypted[0..32].try_into().map_err(|_| {
                Report::new(RandomFileKeyDecodingError)
                    .attach_printable("The final decoded byte array was the wrong length")
            })?,
            PhantomData,
        ))
    }
}

error!(pub CompressionError; "Failed to compress data");
error!(pub DecompressionError; "Failed to decompress data");

fn compress(data: &[u8]) -> Result<Vec<u8>, CompressionError> {
    trace!("Compressing something");

    let mut encoder = CompressorReader::new(data, data.len(), 11, 22);

    let mut compressed = Vec::new();

    encoder
        .read_to_end(&mut compressed)
        .change_context(CompressionError)?;

    Ok(compressed)
}

fn decompress(data: &[u8]) -> Result<Vec<u8>, DecompressionError> {
    trace!("Decompressing something");

    let mut decoder = Decompressor::new(data, data.len());

    let mut decompressed = Vec::new();

    decoder
        .read_to_end(&mut decompressed)
        .change_context(DecompressionError)?;

    Ok(decompressed)
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::{
        transformer::{compress, decompress},
        Key, RandomFileKey, RandomFileKeyName, Section, Text,
    };
    use electra::{managable::NameType, MemoryStorageAccess, Reference};
    use secrecy::Secret;

    type RandomFileKeySection =
        RandomFileKey<Section<MemoryStorageAccess<Reference<Path>, Vec<u8>>>>;

    #[tokio::test]
    async fn random_file_key_transformer_no_encryption() {
        let mut data = [0; 32];

        for (i, v) in data.iter_mut().enumerate() {
            *v = i as u8;
        }

        let encoded =
            RandomFileKeyName::encode(&RandomFileKeySection::from_data(data), None).unwrap();
        let decoded: RandomFileKeySection = RandomFileKeyName::decode(&encoded, None).unwrap();

        assert_eq!(data[..], decoded.0[..]);

        assert!(<RandomFileKeyName as NameType<_, _, Text>>::decode(&encoded, None).is_err());
    }

    #[test]
    fn random_file_key_transformer_encryption() {
        let mut data = [0; 32];

        for (i, v) in data.iter_mut().enumerate() {
            *v = i as u8;
        }

        let key = [0; 32];

        for (i, v) in data.iter_mut().enumerate() {
            *v = i as u8;
        }

        let key = Some(Key::custom(Secret::from(key)));

        let encoded =
            RandomFileKeyName::encode(&RandomFileKeySection::from_data(data), key.to_owned())
                .unwrap();
        let decoded: RandomFileKeySection =
            RandomFileKeyName::decode(&encoded, key.to_owned()).unwrap();

        assert_eq!(data[..], decoded.0[..]);

        assert!(<RandomFileKeyName as NameType<_, _, Text>>::decode(&encoded, key).is_err());
    }

    #[test]
    fn compression() {
        let mut data = [0; 64];

        for (i, v) in data.iter_mut().enumerate() {
            *v = i as u8;
        }

        let compressed = compress(&data).unwrap();

        // Make sure implementation doesn't change
        assert_eq!(
            compressed,
            [
                27, 63, 0, 232, 101, 113, 92, 202, 59, 196, 50, 68, 163, 66, 98, 130, 77, 28, 187,
                251, 11, 8, 19, 202, 184, 144, 74, 27, 235, 124, 136, 41, 151, 218, 250, 152, 107,
                159, 251, 62
            ]
        );

        let decompressed = decompress(&compressed).unwrap();

        assert_eq!(data[..], decompressed[..]);
    }
}
