use std::{path::Path, sync::Arc};

use electra::{
    managable::{Managable, StaticName},
    Reference, StorageAccess,
};
use error_stack::{Result, ResultExt};
use error_stack_error_macro::error;
use futures::{stream, TryStreamExt};
use log::trace;
use serde::{Deserialize, Serialize};

use crate::{Key, RandomFileKey, Section, Sections, TryRepair};

use super::{Categories, Filters};

/// All the diary's metadata that must be encrypted
#[derive(Serialize, Deserialize, Clone)]
#[serde(
    from = "crate::data_versions::MetaSecureVersions",
    into = "crate::data_versions::MetaSecureVersions"
)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct MetaSecure {
    /// All the categories in the diary
    pub categories: Categories,
    /// The current filters for sections
    pub filters: Filters,
}

error!(pub MetaSecureRepairError; "Failed to validate or repair MetaSecure");

impl TryRepair for MetaSecure {
    type Error = MetaSecureRepairError;

    fn try_repair(&mut self) -> Result<(), Self::Error> {
        self.categories
            .try_repair()
            .change_context(MetaSecureRepairError)?;

        self.filters
            .category_selector_mut()
            .repair(&self.categories);

        Ok(())
    }
}

impl Managable<Reference<Path>, Vec<u8>> for MetaSecure {
    type Name = StaticName<Reference<Path>>;

    const STATIC_NAME_INPUT: fn() -> Reference<Path> =
        || Reference::Borrowed("meta_secure.cbor.gz".as_ref());

    type TransformerParams = Option<Key>;

    type EncodeError = crate::MaybeEncryptError;
    type DecodeError = crate::MaybeDecryptError;

    fn encode(&mut self, maybe_key: Option<Key>) -> Result<Vec<u8>, Self::EncodeError> {
        crate::encode_with_encryption(self, &maybe_key)
    }

    fn decode(data: Vec<u8>, maybe_key: Option<Key>) -> Result<Self, Self::DecodeError> {
        crate::decode_with_decryption(&data, &maybe_key)
    }
}

error!(pub SectionsPassingFilterError; "Failed to find the sections passing the filters");

impl MetaSecure {
    /// Create a new `MetaSecure` instance
    pub const fn new(categories: Categories, filters: Filters) -> MetaSecure {
        MetaSecure {
            categories,
            filters,
        }
    }

    /// Get all the sections that pass the diary's filter
    pub async fn sections_passing_filter<
        SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
    >(
        &self,
        sa: Arc<SA>,
        sections: &Sections<SA>,
        maybe_key: Option<Key>,
    ) -> Result<Vec<RandomFileKey<Section<SA>>>, SectionsPassingFilterError> {
        trace!("Getting the sections passing the filters");

        let categories = &self.categories;

        stream::iter(sections.iter().map(Ok))
            .try_filter_map(|(key, value)| async {
                Ok(self
                    .filters
                    .section_matches_filters(
                        Arc::clone(&sa),
                        maybe_key.to_owned(),
                        value,
                        categories,
                    )
                    .await
                    .change_context(SectionsPassingFilterError)?
                    .then_some(*key))
            })
            .try_collect::<Vec<_>>()
            .await
    }

    /// Delete a category from both the categories list & the selected categories
    pub fn delete_category(&mut self, category_id: u32) {
        self.categories.delete_category(category_id);

        self.filters
            .category_selector_mut()
            .remove_category(category_id);
    }
}
