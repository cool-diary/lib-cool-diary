use std::path::Path;

use electra::{managable::Managable, Reference, StorageAccess};
use serde::{Deserialize, Serialize};

use crate::{Meta, MetaSecure, RandomFileKeyName};
mod v1;

impl From<MetaSecure> for MetaSecureVersions {
    fn from(data: MetaSecure) -> Self {
        MetaSecureVersions::V1(v1::MetaSecure::from(data))
    }
}

impl From<MetaSecureVersions> for MetaSecure {
    fn from(value: MetaSecureVersions) -> Self {
        match value {
            MetaSecureVersions::V1(v) => v.into(),
            MetaSecureVersions::VN(v) => match v {},
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub(crate) enum MetaSecureVersions {
    V1(v1::MetaSecure),
    VN(NewMetaSecureVersions),
}

#[derive(Serialize, Deserialize)]
pub(crate) enum NewMetaSecureVersions {}

impl From<Meta> for MetaVersions {
    fn from(data: Meta) -> Self {
        MetaVersions::V1(v1::Meta::from(data))
    }
}

impl From<MetaVersions> for Meta {
    fn from(value: MetaVersions) -> Self {
        match value {
            MetaVersions::V1(v) => v.into(),
            MetaVersions::VN(v) => match v {},
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub(crate) enum MetaVersions {
    V1(v1::Meta),
    VN(NewMetaVersions),
}

#[derive(Serialize, Deserialize)]
pub(crate) enum NewMetaVersions {}

impl SectionVersions {
    pub fn from_ref<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized>(
        value: &crate::Section<SA>,
    ) -> SectionVersions {
        SectionVersions::V1(v1::Section::from_ref(value))
    }
}

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized>
    From<SectionVersions> for crate::Section<SA>
{
    fn from(value: SectionVersions) -> Self {
        match value {
            SectionVersions::V1(v) => v.into(),
            SectionVersions::VN(v) => match v {},
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub(crate) enum SectionVersions {
    V1(crate::data_versions::v1::Section),
    VN(NewSectionVersions),
}

#[derive(Serialize, Deserialize)]
pub(crate) enum NewSectionVersions {}

impl From<crate::Text> for TextVersions {
    fn from(value: crate::Text) -> Self {
        TextVersions::V1(value.into())
    }
}

impl From<TextVersions> for crate::Text {
    fn from(value: TextVersions) -> Self {
        match value {
            TextVersions::V1(v) => v.into(),
            TextVersions::VN(v) => match v {},
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub(crate) enum TextVersions {
    V1(v1::Text),
    VN(NewTextVersions),
}

#[derive(Serialize, Deserialize)]
pub(crate) enum NewTextVersions {}

impl<Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName> + Clone>
    From<crate::Edits<Medium>> for EditsVersions<Medium>
{
    fn from(value: crate::Edits<Medium>) -> Self {
        EditsVersions::V1(value.into())
    }
}

impl<Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName> + Clone>
    From<EditsVersions<Medium>> for crate::Edits<Medium>
{
    fn from(value: EditsVersions<Medium>) -> Self {
        match value {
            EditsVersions::V1(v) => v.into(),
            EditsVersions::VN(v) => match v {},
        }
    }
}

#[derive(Serialize, Deserialize)]
pub(crate) enum EditsVersions<
    Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName> + Clone,
> {
    V1(v1::Edits<Medium>),
    VN(NewEditsVersions),
}

#[derive(Serialize, Deserialize)]
pub(crate) enum NewEditsVersions {}
