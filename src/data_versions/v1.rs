use std::{collections::HashMap, path::Path};

use electra::{managable::Managable, LazyManager, Reference, StorageAccess};
use log::trace;
use serde::{de::DeserializeOwned, Deserialize, Serialize};

use crate::{Categories, Category, RandomFileKey, RandomFileKeyName, TryRepair};

#[derive(Serialize, Deserialize)]
pub struct MetaSecure {
    categories: Vec<CategoryInTree>,
    filters: Filters,
}

impl From<crate::MetaSecure> for MetaSecure {
    fn from(value: crate::MetaSecure) -> Self {
        Self {
            categories: value.categories.into(),
            filters: Filters {
                selected_categories: CategorySelector(
                    value.filters.selected_categories.into_inner(),
                ),
                category_filter_type: match value.filters.category_filter_type {
                    crate::CategoryFilterType::Any => CategoryFilterType::Any,
                    crate::CategoryFilterType::All => CategoryFilterType::All,
                    crate::CategoryFilterType::Not => CategoryFilterType::Not,
                },
            },
        }
    }
}

impl From<MetaSecure> for crate::MetaSecure {
    fn from(value: MetaSecure) -> Self {
        Self {
            categories: value.categories.into(),
            filters: crate::Filters {
                selected_categories: crate::CategorySelector::new_with(
                    value.filters.selected_categories.0,
                ),
                category_filter_type: match value.filters.category_filter_type {
                    CategoryFilterType::Any => crate::CategoryFilterType::Any,
                    CategoryFilterType::All => crate::CategoryFilterType::All,
                    CategoryFilterType::Not => crate::CategoryFilterType::Not,
                },
                time: None,
                text_search: String::new(),
            },
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[cfg_attr(debug_assertions, derive(Debug))]
struct Filters {
    selected_categories: CategorySelector,
    category_filter_type: CategoryFilterType,
}

#[derive(Serialize, Deserialize, Clone)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[repr(transparent)]
#[serde(transparent)]
struct CategorySelector(Vec<u32>);

#[derive(Serialize, Deserialize, Clone, Copy)]
#[cfg_attr(debug_assertions, derive(Debug))]
enum CategoryFilterType {
    Any,
    All,
    Not,
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Eq)]
#[cfg_attr(debug_assertions, derive(Debug))]
enum Time {
    None,
    Date(Date),
    Range((Date, Date)),
}

impl From<crate::Time> for Time {
    fn from(value: crate::Time) -> Self {
        match value {
            crate::Time::None => Time::None,
            crate::Time::Date(date) => Time::Date(date.into()),
            crate::Time::Range(range) => Time::Range((range.start().into(), range.end().into())),
        }
    }
}

impl From<Time> for crate::Time {
    fn from(value: Time) -> Self {
        match value {
            Time::None => Self::None,
            Time::Date(date) => Self::Date(date.into()),
            Time::Range((start, end)) => Self::Range(crate::Range::new(start.into(), end.into())),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Eq)]
#[cfg_attr(debug_assertions, derive(Debug))]
struct Date {
    year: i32,
    month: u8,
    day: u8,
}

impl From<crate::Date> for Date {
    fn from(value: crate::Date) -> Self {
        Self {
            year: value.year,
            month: value.month,
            day: value.day,
        }
    }
}

impl From<Date> for crate::Date {
    fn from(value: Date) -> Self {
        Self {
            year: value.year,
            month: value.month,
            day: value.day,
        }
    }
}

#[derive(Serialize, Deserialize)]
struct CategoryInTree {
    id: u32,
    name: String,
    color: String,
    subcategories: Vec<CategoryInTree>,
    closed: bool,
}

impl CategoryInTree {
    // https://github.com/rust-lang/rust/issues/59618 rip
    fn treeify(&mut self, categories: &mut HashMap<u32, (CategoryInTree, u32)>) {
        let mut subcategory_ids = Vec::new();

        for (id, entry) in categories.iter() {
            if entry.1 == self.id {
                subcategory_ids.push(*id);
            }
        }

        let mut subcategories = Vec::new();

        for id in subcategory_ids {
            if let Some(entry) = categories.remove(&id) {
                subcategories.push(entry.0);
            }
        }

        subcategories.sort_by(|a, b| a.name.cmp(&b.name));

        for mut subcategory in subcategories {
            subcategory.treeify(categories);

            self.subcategories.push(subcategory);
        }
    }

    fn flatten(self, categories: &mut HashMap<u32, Category>, parent: Option<u32>) {
        categories.insert(
            self.id,
            Category {
                name: self.name,
                parent,
                color: self.color,
                closed: self.closed,
            },
        );

        for subcategory in self.subcategories {
            subcategory.flatten(categories, Some(self.id));
        }
    }
}

impl From<Categories> for Vec<CategoryInTree> {
    fn from(val: Categories) -> Self {
        trace!("Treeifying the categories");

        let mut categories = HashMap::new();
        let mut no_parents = Vec::new();

        for (id, category) in val.0 {
            let category_in_tree = CategoryInTree {
                id,
                name: category.name,
                color: category.color,
                subcategories: Vec::new(),
                closed: category.closed,
            };

            match category.parent {
                Some(parent) => {
                    categories.insert(id, (category_in_tree, parent));
                }
                None => {
                    no_parents.push(category_in_tree);
                }
            }
        }

        no_parents.sort_by(|a, b| a.name.cmp(&b.name));

        for category in &mut no_parents {
            category.treeify(&mut categories);
        }

        no_parents
    }
}

impl From<Vec<CategoryInTree>> for Categories {
    fn from(categories: Vec<CategoryInTree>) -> Self {
        trace!("Flattening a category tree");

        let mut map = HashMap::new();

        for category in categories {
            category.flatten(&mut map, None);
        }

        Categories(map)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Meta {
    encryption: Option<([u8; 16], u32)>,
}

impl From<crate::Meta> for Meta {
    fn from(value: crate::Meta) -> Self {
        Self {
            encryption: value.encryption().map(|v| (v.salt, v.rounds)),
        }
    }
}

impl From<Meta> for crate::Meta {
    fn from(value: Meta) -> Self {
        Self::new(value.encryption.map(|v| crate::HashingData {
            salt: v.0,
            rounds: v.1,
        }))
    }
}

#[derive(Serialize, Deserialize)]
pub struct Section {
    title: String,
    categories: SectionCategories,
    files: Vec<Media>,
    ordinal: u64,
    links_to: Vec<[u8; 32]>,
}

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> From<Section>
    for crate::Section<SA>
{
    fn from(value: Section) -> Self {
        crate::Section {
            title: value.title,
            categories: crate::SectionCategories::new(
                value.categories.time.into(),
                crate::CategorySelector::new_with(value.categories.category_ids.0),
            ),
            files: value
                .files
                .into_iter()
                .map(|file| match file {
                    Media::Text(text) => crate::Media::Text(text.into()),
                })
                .collect(),
            ordinal: value.ordinal,
            links_to: value
                .links_to
                .into_iter()
                .map(RandomFileKey::from_data)
                .collect(),
        }
    }
}

impl Section {
    pub fn from_ref<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized>(
        value: &crate::Section<SA>,
    ) -> Section {
        Section {
            title: value.title().to_owned(),
            categories: SectionCategories {
                time: value.categories().time().into(),
                category_ids: CategorySelector(value.categories().category_selector().to_vec()),
            },
            files: value
                .files()
                .iter()
                .map(|file| match file {
                    crate::Media::Text(text) => Media::Text(MediaEdits::from_ref(text)),
                })
                .collect(),
            ordinal: value.ordinal(),
            links_to: value.links_to().iter().map(|v| v.0).collect(),
        }
    }
}

#[derive(Serialize, Deserialize)]
struct SectionCategories {
    time: Time,
    category_ids: CategorySelector,
}

#[derive(Serialize, Deserialize)]
pub enum Media {
    /// A text file
    Text(MediaEdits),
}

#[derive(Serialize, Deserialize)]
pub struct MediaEdits {
    latest_time: u64,
    latest: [u8; 32],
    edits: [u8; 32],
}

impl MediaEdits {
    const fn from_ref<
        Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName>
            + TryRepair
            + Serialize
            + DeserializeOwned
            + Clone,
        SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
    >(
        value: &crate::MediaEdits<Medium, SA>,
    ) -> MediaEdits {
        MediaEdits {
            latest_time: value.latest_time,
            latest: value.latest.name_input().0,
            edits: value.edits.name_input().0,
        }
    }
}

impl<
        Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName>
            + TryRepair
            + Serialize
            + DeserializeOwned
            + Clone,
        SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
    > From<MediaEdits> for crate::MediaEdits<Medium, SA>
{
    fn from(value: MediaEdits) -> Self {
        Self {
            latest_time: value.latest_time,
            latest: LazyManager::from_name_unchecked(RandomFileKey::from_data(value.latest)),
            edits: LazyManager::from_name_unchecked(RandomFileKey::from_data(value.edits)),
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(transparent)]
pub struct Text(String);

impl From<Text> for crate::Text {
    fn from(value: Text) -> Self {
        crate::Text::from(value.0)
    }
}

impl From<crate::Text> for Text {
    fn from(value: crate::Text) -> Self {
        Text(value.into())
    }
}

#[derive(Serialize, Deserialize)]
pub(crate) struct Edits<Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName>>(
    Vec<(u64, Medium)>,
);

impl<Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName> + Clone>
    From<crate::Edits<Medium>> for Edits<Medium>
{
    fn from(value: crate::Edits<Medium>) -> Self {
        Self(value.0)
    }
}

impl<Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName> + Clone>
    From<Edits<Medium>> for crate::Edits<Medium>
{
    fn from(value: Edits<Medium>) -> Self {
        Self(value.0)
    }
}

#[cfg(test)]
mod tests {
    use crate::{data_versions::v1::CategoryInTree, Categories};

    fn dummy_categories() -> Categories {
        let mut categories = Categories::default();

        let (id0, _cat0) = categories.create_category(String::new());

        let (id1, cat1) = categories.create_category(String::new());
        cat1.set_parent(Some(id0));

        let (_, cat2) = categories.create_category(String::new());
        cat2.set_parent(Some(id1));

        let (_, cat3) = categories.create_category(String::new());
        cat3.set_parent(Some(id1));

        categories
    }

    #[test]
    fn category_and_tree_conversion() {
        let categories = dummy_categories();

        let tree: Vec<CategoryInTree> = categories.clone().into();

        assert_eq!(tree.len(), 1);

        let base = &tree[0];
        assert_eq!(base.id, 1);
        assert_eq!(base.subcategories.len(), 1);

        let cat_in_tree1 = &base.subcategories[0];
        assert_eq!(cat_in_tree1.id, 2);
        assert_eq!(cat_in_tree1.subcategories.len(), 2);

        let cat_in_tree2 = &cat_in_tree1.subcategories[0];
        let cat_in_tree3 = &cat_in_tree1.subcategories[1];
        assert!(
            cat_in_tree2.id == 3 && cat_in_tree3.id == 4
                || cat_in_tree2.id == 4 && cat_in_tree3.id == 3
        );

        assert_eq!(
            <Vec<CategoryInTree> as Into<Categories>>::into(tree),
            categories
        );
    }
}
