use std::{convert::Infallible, fmt::Debug, mem, ops::Deref, path::Path, sync::Arc, vec::Vec};

use electra::{
    managable::Managable, DeleteLazyManagerError, LazyManager, Manager, ManagerError, Reference,
    StorageAccess,
};
use error_stack::{Report, Result, ResultExt};
use error_stack_error_macro::error;
use futures::{
    future::join,
    stream::{self, StreamExt},
    Future, TryStreamExt,
};
use log::{debug, trace};
use serde::{de::DeserializeOwned, Deserialize, Serialize};

use crate::{
    decode_with_decryption, encode_with_encryption, CategorySelector, Key, MaybeDecryptError,
    MaybeEncryptError, RandomFileKey, RandomFileKeyName, Sections, TryRepair,
};

use super::{Filters, Time};

/// The categories & time associated with a section
#[derive(Clone)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct SectionCategories {
    time: Time,
    category_ids: CategorySelector,
}

error!(pub RepairSectionCategoriesError; "Failed to validate or repair section");

impl TryRepair for SectionCategories {
    type Error = RepairSectionCategoriesError;

    fn try_repair(&mut self) -> Result<(), RepairSectionCategoriesError> {
        self.time
            .try_repair()
            .change_context(RepairSectionCategoriesError)
    }
}

impl SectionCategories {
    /// Create a new `SectionCategories` instance
    pub const fn new(time: Time, category_selector: CategorySelector) -> SectionCategories {
        SectionCategories {
            time,
            category_ids: category_selector,
        }
    }

    /// Get the time associated with the section
    pub const fn time(&self) -> Time {
        self.time
    }

    /// Get the category selector associated with the section
    pub const fn category_selector(&self) -> &CategorySelector {
        &self.category_ids
    }

    /// Get a mutable category selector associated with the section
    pub fn category_selector_mut(&mut self) -> &mut CategorySelector {
        &mut self.category_ids
    }

    /// Set the time associated with the section
    pub fn set_time(&mut self, time: Time) {
        self.time = time;
    }
}

/// Represents a section
#[derive(Deserialize)]
#[serde(bound = "", from = "crate::data_versions::SectionVersions")]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Section<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> {
    pub(crate) title: String,
    pub(crate) categories: SectionCategories,
    pub(crate) files: Vec<Media<SA>>,
    pub(crate) ordinal: u64,
    pub(crate) links_to: Vec<RandomFileKey<Section<SA>>>,
}

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> Serialize
    for Section<SA>
{
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        crate::data_versions::SectionVersions::from_ref(self).serialize(serializer)
    }
}

error!(pub RepairSectionError; "Failed to validate or repair a section");

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> TryRepair
    for Section<SA>
{
    type Error = RepairSectionError;

    fn try_repair(&mut self) -> Result<(), RepairSectionError> {
        self.categories
            .try_repair()
            .change_context(RepairSectionError)
    }
}

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized>
    Managable<Reference<Path>, Vec<u8>> for Section<SA>
{
    type Name = RandomFileKeyName;

    const STATIC_NAME_INPUT: &'static [u8; 8] = b"section ";

    type TransformerParams = Option<Key>;

    type EncodeError = crate::MaybeEncryptError;
    type DecodeError = crate::MaybeDecryptError;

    fn encode(&mut self, maybe_key: Option<Key>) -> Result<Vec<u8>, Self::EncodeError> {
        crate::encode_with_encryption(self, &maybe_key)
    }

    fn decode(data: Vec<u8>, maybe_key: Option<Key>) -> Result<Self, Self::DecodeError> {
        crate::decode_with_decryption(&*data, &maybe_key)
    }
}

/// A piece of media that represents text
#[derive(Serialize, Deserialize, Clone)]
#[serde(
    from = "crate::data_versions::TextVersions",
    into = "crate::data_versions::TextVersions"
)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Text(String);

impl From<String> for Text {
    fn from(value: String) -> Self {
        Text(value)
    }
}

impl From<Text> for String {
    fn from(value: Text) -> Self {
        value.0
    }
}

impl Deref for Text {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl TryRepair for Text {
    type Error = Infallible;

    fn try_repair(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl Managable<Reference<Path>, Vec<u8>> for Text {
    type Name = RandomFileKeyName;

    const STATIC_NAME_INPUT: <Self::Name as electra::managable::NameType<
        Reference<Path>,
        Vec<u8>,
        Self,
    >>::StaticInput = b"text    ";

    type TransformerParams = Option<Key>;

    type EncodeError = crate::MaybeEncryptError;
    type DecodeError = crate::MaybeDecryptError;

    fn encode(&mut self, maybe_key: Option<Key>) -> Result<Vec<u8>, Self::EncodeError> {
        crate::encode_with_encryption(self, &maybe_key)
    }

    fn decode(data: Vec<u8>, maybe_key: Option<Key>) -> Result<Self, Self::DecodeError> {
        crate::decode_with_decryption(&data, &maybe_key)
    }
}

/// Types of media that can be included in a `SectionEdit`
#[cfg_attr(debug_assertions, derive(Debug))]
pub enum Media<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> {
    /// A text file
    Text(MediaEdits<Text, SA>),
}

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> Media<SA> {
    /// Delete the files backing the media
    pub async fn delete(self, sa: &SA, key: Option<Key>) -> Result<(), DeleteLazyManagerError> {
        match self {
            Media::Text(text) => text.delete(sa, key).await,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(
    from = "crate::data_versions::EditsVersions<Medium>",
    into = "crate::data_versions::EditsVersions<Medium>"
)]
pub(crate) struct Edits<
    Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName> + Clone,
>(pub(crate) Vec<(u64, Medium)>);

impl<Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName> + TryRepair + Clone>
    TryRepair for Edits<Medium>
{
    type Error = <Medium as TryRepair>::Error;

    fn try_repair(&mut self) -> Result<(), Self::Error> {
        for section in &mut self.0 {
            section.1.try_repair()?;
        }

        Ok(())
    }
}

impl<
        Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName>
            + TryRepair
            + Serialize
            + DeserializeOwned
            + Clone,
    > Managable<Reference<Path>, Vec<u8>> for Edits<Medium>
{
    type Name = RandomFileKeyName;

    const STATIC_NAME_INPUT: <Self::Name as electra::managable::NameType<
        Reference<Path>,
        Vec<u8>,
        Self,
    >>::StaticInput = b"edits   ";

    type TransformerParams = Option<Key>;

    type EncodeError = MaybeEncryptError;
    type DecodeError = MaybeDecryptError;

    fn encode(&mut self, params: Self::TransformerParams) -> Result<Vec<u8>, Self::EncodeError> {
        encode_with_encryption(self, &params)
    }

    fn decode(data: Vec<u8>, params: Self::TransformerParams) -> Result<Self, Self::DecodeError> {
        decode_with_decryption(&data, &params)
    }
}

/// Represents one of the section's files & points to its contents
pub struct MediaEdits<
    Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName>
        + TryRepair
        + Serialize
        + DeserializeOwned
        + Clone,
    SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
> {
    pub(crate) latest_time: u64,
    pub(crate) latest: LazyManager<Medium, SA>,
    pub(crate) edits: LazyManager<Edits<Medium>, SA>,
}

#[cfg(debug_assertions)]
impl<
        Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName>
            + TryRepair
            + Serialize
            + DeserializeOwned
            + Clone,
        SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
    > Debug for MediaEdits<Medium, SA>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("MediaEdits")
            .field("latest_time", &self.latest_time)
            .field("latest", &self.latest)
            .field("edits", &self.edits)
            .finish()
    }
}

error!(pub NewMediaEditsError; "Failed to create a new MediaEdits");
error!(pub LoadMediaError; "Failed to load a media");

impl<
        Medium: Managable<Reference<Path>, Vec<u8>, Name = RandomFileKeyName>
            + TryRepair
            + Serialize
            + DeserializeOwned
            + Clone,
        SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
    > MediaEdits<Medium, SA>
{
    /// Create a new `MediaEdits` instance
    pub async fn new(
        sa: Arc<SA>,
        key: Option<Key>,
        now: u64,
        latest: Medium,
        params: Medium::TransformerParams,
    ) -> Result<MediaEdits<Medium, SA>, NewMediaEditsError> {
        let edits_id = RandomFileKey::generate(&*sa, key.to_owned())
            .await
            .change_context(NewMediaEditsError)?;

        let edits = LazyManager::from_manager(
            edits_id,
            Manager::new_from_data(
                Arc::clone(&sa),
                &edits_id,
                Edits(Vec::new()),
                key.to_owned(),
                key.to_owned(),
            )
            .await
            .change_context(NewMediaEditsError)?,
        );

        let latest_id = RandomFileKey::generate(&*sa, key.to_owned())
            .await
            .change_context(NewMediaEditsError)?;

        let latest = LazyManager::from_manager(
            latest_id,
            Manager::new_from_data(Arc::clone(&sa), &latest_id, latest, params, key)
                .await
                .change_context(NewMediaEditsError)?,
        );

        Ok(MediaEdits {
            latest_time: now,
            latest,
            edits,
        })
    }

    /// Load the media's data into memory from the file
    pub async fn load(
        &self,
        sa: Arc<SA>,
        params: Medium::TransformerParams,
        key: Option<Key>,
    ) -> Result<bool, LoadMediaError> {
        self.latest
            .load(sa, params, key)
            .await
            .change_context(LoadMediaError)
    }

    /// Load the media's edit history into memory from the file (Needed when doing updates)
    pub async fn load_edits(&self, sa: Arc<SA>, key: Option<Key>) -> Result<bool, LoadMediaError> {
        self.edits
            .load(sa, key.to_owned(), key)
            .await
            .change_context(LoadMediaError)
    }

    /// Update the contents of the file
    pub fn update(
        &mut self,
        medium: Medium,
        now: u64,
        params: Medium::TransformerParams,
        key: Option<Key>,
    ) -> Option<impl Future<Output = Result<(), ManagerError>>>
    where
        Medium::TransformerParams: Clone,
    {
        debug!("Updating a section's media");

        let (write_future, prev) = self
            .latest
            .try_get_mut()?
            .write(params, |prev| (medium, prev));

        let maybe_write_future_2 = if self.latest_time < now - 60 * 60 {
            let edits = self.edits.try_get_mut()?;

            Some(
                edits
                    .write(key, move |mut edits| {
                        edits.0.push((now, prev));

                        (edits, ())
                    })
                    .0,
            )
        } else {
            None
        };

        self.latest_time = now;

        Some(async {
            match maybe_write_future_2 {
                Some(future) => {
                    let (a, b) = join(write_future, future).await;
                    a?;
                    b
                }
                None => write_future.await,
            }
        })
    }

    /// Get the latest edit in the file
    pub fn latest(&self) -> Option<&Medium> {
        self.latest.try_get().map(|v| &**v)
    }

    /// Get the past edist of the file
    pub fn past_edits(&self) -> Option<&[(u64, Medium)]> {
        self.edits.try_get().map(|v| &*v.0)
    }

    /// Delete the `MediaEdits`
    pub async fn delete(self, sa: &SA, key: Option<Key>) -> Result<(), DeleteLazyManagerError> {
        self.latest.delete(sa, key.to_owned()).await?;
        self.edits.delete(sa, key).await
    }
}

error!(pub SectionCreateError; "Failed to create a new section");
error!(pub UpdateSectionTextError; "Failed to update a section's text");
error!(pub DeleteFileError; "Failed to delete a section file");

impl<SA: ?Sized + StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>>> Section<SA> {
    /// Create a new section
    pub fn new(filters: &Filters, sections: &Sections<SA>) -> Section<SA> {
        trace!("Creating a new section");
        Section {
            categories: SectionCategories::new(
                filters.time().unwrap_or(Time::None),
                filters.category_selector().clone(),
            ),
            title: String::new(),
            files: Vec::new(),
            ordinal: get_next_ord(sections),
            links_to: vec![],
        }
    }

    /// Swap positions of this section with another, in terms of how they're sorted
    pub fn swap_places_with(&mut self, other: &mut Section<SA>) {
        mem::swap(&mut other.ordinal, &mut self.ordinal);
    }

    /// Get the section's ordinal (where it should sorted)
    pub const fn ordinal(&self) -> u64 {
        self.ordinal
    }

    /// Change the sections `SectionCategories`
    pub fn categories_mut(&mut self) -> &mut SectionCategories {
        &mut self.categories
    }

    /// Set the section's title
    pub fn set_title(&mut self, title: String) {
        self.title = title;
    }

    /// Get the section's title
    pub fn title(&self) -> &str {
        &self.title
    }

    /// Get the sections `SectionCategories`
    pub const fn categories(&self) -> &SectionCategories {
        &self.categories
    }

    /// Get all of the section's files
    pub fn files(&self) -> &[Media<SA>] {
        self.files.as_ref()
    }

    /// Get a section file by index
    pub fn get_file(&self, idx: usize) -> Option<&Media<SA>> {
        self.files().get(idx)
    }

    /// Get a mutable list of the section's files
    pub fn files_mut(&mut self) -> &mut [Media<SA>] {
        &mut self.files
    }

    /// Get a section file by index mutably
    pub fn update_file(&mut self, idx: usize) -> Option<&mut Media<SA>> {
        self.files_mut().get_mut(idx)
    }

    /// Add a file to the section
    #[allow(clippy::missing_panics_doc)]
    pub fn add_file(&mut self, file: Media<SA>) -> &mut Media<SA> {
        self.files.push(file);
        self.files.last_mut().expect("it was just pushed")
    }

    /// Delete a file in the section
    pub async fn remove_file(
        &mut self,
        sa: &SA,
        key: Option<Key>,
        idx: usize,
    ) -> Result<(), DeleteFileError> {
        if self.files.len() <= idx {
            return Err(Report::new(DeleteFileError)
                .attach_printable(format!("The index {idx} doesn't exist on the section")));
        }

        self.files
            .remove(idx)
            .delete(sa, key)
            .await
            .change_context(DeleteFileError)
    }

    /// Delete all of the files in the section
    pub fn remove_all_files(
        &mut self,
        sa: Arc<SA>,
        key: Option<Key>,
    ) -> impl Future<Output = Result<(), DeleteLazyManagerError>> {
        let iter = stream::iter(std::mem::take(&mut self.files));

        async move {
            iter.then(|file| file.delete(&sa, key.to_owned()))
                .try_collect::<()>()
                .await
        }
    }

    /// Get keys to the sections this section links to
    pub fn links_to(&self) -> &[RandomFileKey<Section<SA>>] {
        &self.links_to
    }
}

fn get_next_ord<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized>(
    sm: &Sections<SA>,
) -> u64 {
    trace!("Getting an ordinal for a new section");

    sm.values().fold(0, |a, v| a.max(v.ordinal())) + 1
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use electra::{all_of_type, Manager, MemoryStorageAccess};

    use crate::{
        get_sections, CategorySelector, Filters, Media, MediaEdits, RandomFileKey, Section, Text,
    };

    #[tokio::test]
    async fn sections() {
        let sa = Arc::new(MemoryStorageAccess::default());

        let filters = Filters::new(
            CategorySelector::new(),
            crate::CategoryFilterType::Any,
            None,
        );

        {
            let sections = get_sections(Arc::clone(&sa), None).await.unwrap();

            let id = RandomFileKey::generate(&*sa, None).await.unwrap();

            let mut section = Manager::new_from_data(
                Arc::clone(&sa),
                &id,
                Section::new(&filters, &sections),
                None,
                None,
            )
            .await
            .unwrap();

            let media_edits =
                MediaEdits::new(Arc::clone(&sa), None, 0, Text::from("oof".to_owned()), None)
                    .await
                    .unwrap();

            section
                .write(None, |mut section| {
                    section.add_file(Media::Text(media_edits));

                    (section, ())
                })
                .0
                .await
                .unwrap();

            assert_eq!(
                match section.files().first().unwrap() {
                    Media::Text(v) => v,
                }
                .latest()
                .unwrap()
                .0,
                "oof"
            );
        }

        {
            let sections = get_sections(Arc::clone(&sa), None).await.unwrap();

            assert_eq!(sections.len(), 1);

            let mut section = sections.into_values().next().unwrap();

            let text_file = match section.get_file(0).unwrap() {
                Media::Text(text) => text,
            };

            text_file.load(Arc::clone(&sa), None, None).await.unwrap();
            text_file.load_edits(Arc::clone(&sa), None).await.unwrap();

            assert_eq!(text_file.latest().unwrap().0, "oof");

            assert_eq!(text_file.past_edits().unwrap().len(), 0);

            assert!(all_of_type::<Text, _>(Arc::clone(&sa), None)
                .await
                .unwrap()
                .next()
                .is_some());

            let text_file = match section.get_file(0).unwrap() {
                Media::Text(text) => text,
            };

            text_file.load(Arc::clone(&sa), None, None).await.unwrap();
            text_file.load_edits(Arc::clone(&sa), None).await.unwrap();

            section
                .write(None, |mut section| {
                    let text_file = match section.update_file(0).unwrap() {
                        Media::Text(text) => text,
                    };

                    let future = text_file
                        .update(Text::from("pog".to_owned()), 100_000, None, None)
                        .unwrap();

                    (section, future)
                })
                .1
                .await
                .unwrap();

            let text_file = match section.get_file(0).unwrap() {
                Media::Text(text) => text,
            };

            assert_eq!(text_file.latest().unwrap().0, "pog");

            let edits = text_file.past_edits().unwrap();

            assert_eq!(edits.len(), 1);
            assert_eq!(edits.first().unwrap().1 .0, "oof");

            section
                .write(None, |mut section| {
                    let text_file = match section.update_file(0).unwrap() {
                        Media::Text(text) => text,
                    };

                    let future = text_file
                        .update(Text::from("yeet".to_owned()), 100_001, None, None)
                        .unwrap();

                    (section, future)
                })
                .1
                .await
                .unwrap();

            let text_file = match section.get_file(0).unwrap() {
                Media::Text(text) => text,
            };

            assert_eq!(text_file.latest().unwrap().0, "yeet");

            let edits = text_file.past_edits().unwrap();

            assert_eq!(edits.len(), 1);
            assert_eq!(edits.first().unwrap().1 .0, "oof");
        }

        {
            let sections = get_sections(Arc::clone(&sa), None).await.unwrap();

            assert_eq!(sections.len(), 1);

            let mut section = sections.into_values().next().unwrap();

            let (write_future, update_future) = section.write(None, |mut v| {
                let ret = v.remove_all_files(Arc::clone(&sa), None);

                (v, ret)
            });

            write_future.await.unwrap();
            update_future.await.unwrap();

            assert!(all_of_type::<Text, _>(Arc::clone(&sa), None)
                .await
                .unwrap()
                .next()
                .is_none());

            section.delete().await.unwrap();

            let sections = get_sections(Arc::clone(&sa), None).await.unwrap();

            assert_eq!(sections.len(), 0);
        }
    }
}
