use std::path::{Path, PathBuf};

use error_stack::{Result, ResultExt};
use error_stack_error_macro::error;
use serde::{de::DeserializeOwned, Deserialize, Serialize};

use electra::{
    managable::{Managable, StaticName},
    Reference,
};

use crate::TryRepair;

/// Represents the application data independent from individual diaries
///
/// Includes a section for custom data (language)
#[derive(Serialize, Deserialize, Debug)]
pub struct Config<Custom> {
    diaries: Vec<(String, PathBuf)>,
    #[serde(bound(
        serialize = "Custom: Serialize",
        deserialize = "Custom: DeserializeOwned"
    ))]
    custom: Custom,
}

impl<Custom> Config<Custom> {
    /// Create a new `Config` instance
    pub const fn new(custom: Custom) -> Self {
        Self {
            diaries: vec![],
            custom,
        }
    }

    /// Get all the application's diaries
    pub fn diaries(&self) -> &[(String, PathBuf)] {
        self.diaries.as_ref()
    }

    /// Change the application's diaries
    pub fn diaries_mut(&mut self) -> &mut Vec<(String, PathBuf)> {
        &mut self.diaries
    }

    /// Get the custom data
    pub const fn custom(&self) -> &Custom {
        &self.custom
    }

    /// Change the custom data
    pub fn custom_mut(&mut self) -> &mut Custom {
        &mut self.custom
    }
}

error!(pub ConfigRepairError; "Failed to validate or repair Config");

impl<Custom: TryRepair + Send> TryRepair for Config<Custom> {
    type Error = ConfigRepairError;

    fn try_repair(&mut self) -> error_stack::Result<(), Self::Error> {
        self.custom.try_repair().change_context(ConfigRepairError)
    }
}

impl<Custom> Managable<Reference<Path>, Vec<u8>> for Config<Custom>
where
    Self: Serialize + DeserializeOwned + TryRepair + Send + Sync + 'static,
{
    type Name = StaticName<Reference<Path>>;

    const STATIC_NAME_INPUT: fn() -> Reference<Path> =
        || Reference::Borrowed("config.cbor.gz".as_ref());

    type TransformerParams = ();

    type EncodeError = crate::EncodeError;
    type DecodeError = crate::DecodeError;

    fn encode(&mut self, (): ()) -> Result<Vec<u8>, Self::EncodeError> {
        crate::encode(self)
    }

    fn decode(data: Vec<u8>, (): ()) -> Result<Self, Self::DecodeError> {
        crate::decode(&data)
    }
}
