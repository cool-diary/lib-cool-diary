use std::{collections::HashMap, mem, path::Path, result, sync::Arc, vec::Vec};

use electra::{all_of_type, LazyManager, Manager, NiceErrorMessage, Reference, StorageAccess};
use error_stack::{Context, Report, Result, ResultExt};
use error_stack_error_macro::error;
use futures::{
    stream::{self, FuturesUnordered},
    Future, StreamExt, TryStreamExt,
};
use log::info;
use tokio::runtime::Handle;

use crate::{
    encryption::{HashingData, Key},
    MetaSecure, RandomFileKey, Section,
};

use super::Meta;

/// A hashmap from `RandomFileKey`s to `LazyManager`s of `Section`s
pub type Sections<SA> = HashMap<RandomFileKey<Section<SA>>, Manager<Section<SA>, SA>>;

error!(pub ReadSectionsError; "Failed to read the sections");

pub(crate) async fn get_sections<
    SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
>(
    sa: Arc<SA>,
    key: Option<Key>,
) -> Result<Sections<SA>, ReadSectionsError> {
    let key_for_unwrapper = key.to_owned();

    stream::iter(
        all_of_type(Arc::clone(&sa), key.to_owned())
            .await
            .change_context(ReadSectionsError)?,
    )
    .then(move |v: LazyManager<Section<SA>, SA>| {
        let sa_for_unwrapper = Arc::clone(&sa);
        let key_for_unwrapper = key_for_unwrapper.to_owned();

        async move {
            Ok((
                v.name_input().to_owned(),
                v.load_unwrap(
                    sa_for_unwrapper,
                    key_for_unwrapper.to_owned(),
                    key_for_unwrapper,
                )
                .await
                .change_context(ReadSectionsError)?,
            ))
        }
    })
    .try_fold(HashMap::new(), |mut map, (name_input, v)| async move {
        map.insert(name_input, v);
        Ok(map)
    })
    .await
}

/// The possible states the diary can be in
pub enum DiaryState<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> {
    /// There is no diary
    None(NoDiary<SA>),
    /// The diary's data is encrypted
    Encrypted(EncryptedDiary<SA>),
    /// The diary is either decrypted or wasn't encrypted in the first place. All of the functionality is accessible
    Decrypted(DecryptedDiary<SA>),
}

error!(pub ReadDiaryError; "Failed to read the diary's state");

error!(pub UpdateStateError; "Failed to update the diary's state");

error!(pub FileDoesntExist { file: &'static str } "The file {file} doesn't exist");

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> DiaryState<SA> {
    /// Get the current diary state from the persistent storage
    pub async fn get(
        storage_access: Arc<SA>,
        handle: Handle,
    ) -> Result<DiaryState<SA>, ReadDiaryError> {
        info!("Getting the diary state");

        let maybe_meta = Manager::<Meta, _>::new(Arc::clone(&storage_access), &(), (), ())
            .await
            .change_context(ReadDiaryError)?;

        let Some(meta) = maybe_meta else {
            info!("The meta file didn't exist, assuming the diary doesn't exist");
            return Ok(DiaryState::None(NoDiary {
                storage_access,
                handle,
            }));
        };

        if meta.encryption().is_some() {
            info!("The meta file indicated that encryption is enabled");
            Ok(DiaryState::Encrypted(EncryptedDiary {
                storage_access,
                meta,
                handle,
            }))
        } else {
            info!("The meta file indicated that encryption is disabled");

            let meta_secure: Manager<MetaSecure, _> =
                Manager::new(Arc::clone(&storage_access), &(), None, ())
                    .await
                    .change_context(ReadDiaryError)?
                    .ok_or_else(|| {
                        NiceErrorMessage::attach(
                            Report::new(ReadDiaryError),
                            FileDoesntExist {
                                file: "meta_secure.cbor.gx",
                            },
                        )
                    })?;

            let decrypted = DecryptedDiary {
                storage_access: Arc::clone(&storage_access),
                meta,
                meta_secure,
                sections: get_sections(storage_access, None)
                    .await
                    .change_context(ReadDiaryError)?,
                maybe_key: None,
                handle,
            };

            Ok(DiaryState::Decrypted(decrypted))
        }
    }

    /// Get the diary's storage access
    pub fn storage_access(&self) -> Arc<SA> {
        match self {
            DiaryState::None(v) => Arc::clone(&v.storage_access),
            DiaryState::Encrypted(v) => Arc::clone(&v.storage_access),
            DiaryState::Decrypted(v) => Arc::clone(&v.storage_access),
        }
    }

    /// Get the diary's Tokio handle
    pub fn handle(&self) -> Handle {
        match self {
            DiaryState::None(v) => v.handle.to_owned(),
            DiaryState::Encrypted(v) => v.handle.to_owned(),
            DiaryState::Decrypted(v) => v.handle.to_owned(),
        }
    }

    #[allow(clippy::future_not_send)] // Caused by lack of Send bounds
    /// Update the diary's state
    pub async fn update<
        E: Context,
        F: Future<Output = result::Result<Self, (Self, Report<E>)>>,
        U: FnOnce(Self) -> F,
    >(
        &mut self,
        update: U,
    ) -> Result<(), UpdateStateError> {
        info!("Updating the data state");

        let tmp = DiaryState::None(NoDiary {
            storage_access: self.storage_access(),
            handle: self.handle(),
        });

        let state = mem::replace(self, tmp);

        match update(state).await {
            Ok(v) => {
                *self = v;
                Ok(())
            }
            Err(e) => {
                *self = e.0;
                Err(e.1.change_context(UpdateStateError))
            }
        }
    }
}

/// The data associated with the diary not existing
pub struct NoDiary<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> {
    /// The persistent storage location
    pub storage_access: Arc<SA>,
    /// The tokio runtime handle
    pub handle: Handle,
}

error!(pub CreateDiaryError; "Failed to create a new diary");

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> NoDiary<SA> {
    /// Create a diary in the persistent storage
    pub async fn create(
        self,
        encryption: Option<(Key, HashingData)>,
        meta_secure: MetaSecure,
        handle: Handle,
    ) -> result::Result<DecryptedDiary<SA>, (DiaryState<SA>, Report<CreateDiaryError>)> {
        info!("Creating a new diary");

        let (meta, maybe_key) = match encryption {
            Some((key, data)) => (Meta::new(Some(data)), Some(key)),
            None => (Meta::new(None), None),
        };

        Ok(DecryptedDiary {
            storage_access: Arc::clone(&self.storage_access),
            meta: match Manager::new_from_data(Arc::clone(&self.storage_access), &(), meta, (), ())
                .await
                .change_context(CreateDiaryError)
            {
                Ok(v) => v,
                Err(e) => return Err((DiaryState::None(self), e)),
            },
            meta_secure: match Manager::new_from_data(
                Arc::clone(&self.storage_access),
                &(),
                meta_secure,
                maybe_key.to_owned(),
                (),
            )
            .await
            .change_context(CreateDiaryError)
            {
                Ok(v) => v,
                Err(e) => return Err((DiaryState::None(self), e)),
            },
            sections: get_sections(Arc::clone(&self.storage_access), maybe_key.to_owned())
                .await
                .change_context(CreateDiaryError)
                .map_err(|e| (DiaryState::None(self), e))?,
            maybe_key,
            handle,
        })
    }
}

/// The data associated with the diary being encrypted
pub struct EncryptedDiary<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized>
{
    /// The persistent storage
    pub storage_access: Arc<SA>,
    /// The unencrypted metadata
    pub meta: Manager<Meta, SA>,
    /// The tokio runtime handle
    pub handle: Handle,
}

error!(pub DecryptDiaryError; "Failed to decrypt diary");

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> EncryptedDiary<SA> {
    /// Decrypt the diary
    pub async fn decrypt(
        self,
        key: Key,
    ) -> result::Result<DecryptedDiary<SA>, (DiaryState<SA>, Report<DecryptDiaryError>)> {
        info!("Attempting to decrypt the diary");

        let decrypted = DecryptedDiary {
            storage_access: Arc::clone(&self.storage_access),
            meta_secure: match Manager::new(
                Arc::clone(&self.storage_access),
                &(),
                Some(key.to_owned()),
                (),
            )
            .await
            .change_context(DecryptDiaryError)
            .and_then(|v| {
                v.ok_or_else(|| {
                    NiceErrorMessage::attach(
                        Report::new(DecryptDiaryError),
                        FileDoesntExist {
                            file: "meta_secure.cbor.gz",
                        },
                    )
                })
            }) {
                Ok(v) => v,
                Err(e) => return Err((DiaryState::Encrypted(self), e)),
            },
            sections: match get_sections(Arc::clone(&self.storage_access), Some(key.to_owned()))
                .await
                .change_context(DecryptDiaryError)
            {
                Ok(v) => v,
                Err(e) => return Err((DiaryState::Encrypted(self), e)),
            },
            meta: self.meta,
            maybe_key: Some(key),
            handle: self.handle,
        };

        Ok(decrypted)
    }
}

/// The data associated with the diary being decrypted
pub struct DecryptedDiary<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized>
{
    /// The persistent storage location
    pub storage_access: Arc<SA>,
    /// The unencrypted metadata
    pub meta: Manager<Meta, SA>,
    /// The secure metadata
    pub meta_secure: Manager<MetaSecure, SA>,
    /// The section manager
    pub sections: Sections<SA>,
    /// Contains the encryption key if encryption is enabled, otherwise it contains None
    pub maybe_key: Option<Key>,
    /// The runtime handle
    pub handle: Handle,
}

error!(pub ChangeEncryptionError; "Failed to change encryption");

impl<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized> DecryptedDiary<SA> {
    /// Change the diary's encryption
    pub async fn change_encryption(
        mut self,
        new_key: Option<Key>,
        new_hashing_data: Option<HashingData>,
    ) -> result::Result<DecryptedDiary<SA>, (DiaryState<SA>, Report<ChangeEncryptionError>)> {
        info!("Changing the diary's encryption");

        match self.change_encryption_impl(new_key, new_hashing_data).await {
            Ok(f) => f,
            Err(e) => {
                return Err((
                    DiaryState::Decrypted(self),
                    e.change_context(ChangeEncryptionError),
                ))
            }
        };

        Ok(self)
    }

    async fn change_encryption_impl(
        &mut self,
        new_key: Option<Key>,
        new_hashing_data: Option<HashingData>,
    ) -> Result<(), ChangeEncryptionError> {
        let new_key_cloned = new_key.to_owned();

        let meta_secure_future = async {
            self.meta_secure
                .write(new_key_cloned, |v| (v, ()))
                .0
                .await
                .change_context(ChangeEncryptionError)
        };

        let new_key_cloned = new_key.to_owned();

        let sections_futures = self
            .sections
            .iter_mut()
            .map(move |(file_key, section)| {
                let new_key_cloned = new_key_cloned.to_owned();

                async move {
                    section
                        .write(new_key_cloned.to_owned(), |v| (v, ()))
                        .0
                        .await
                        .change_context(ChangeEncryptionError)?;
                    section
                        .rename(file_key, new_key_cloned)
                        .await
                        .change_context(ChangeEncryptionError)?;

                    Ok(())
                }
            })
            .collect::<FuturesUnordered<_>>();

        self.meta
            .write((), move |mut meta| {
                meta.set_encryption(new_hashing_data);

                (meta, ())
            })
            .0
            .await
            .change_context(ChangeEncryptionError)?;

        self.maybe_key = new_key;

        tokio::try_join!(meta_secure_future, sections_futures.try_collect::<()>())
            .change_context(ChangeEncryptionError)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::{path::Path, sync::Arc};

    use secrecy::Secret;

    use electra::{Manager, MemoryStorageAccess, Reference, StorageAccess};

    use crate::{
        encryption::{encryption_data_from_password, Key},
        Categories, CategoryFilterType, CategorySelector, DecryptedDiary, DiaryState, Filters,
        MetaSecure, RandomFileKey, Section,
    };

    async fn test_diary<SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>>>(
        storage_access: Arc<SA>,
    ) -> DecryptedDiary<SA> {
        let state = DiaryState::get(storage_access, tokio::runtime::Handle::current())
            .await
            .unwrap();

        let (key, hashing_data) = encryption_data_from_password(
            &tokio::runtime::Handle::current(),
            Secret::new(b"bruh".to_vec()),
            1,
        )
        .await
        .unwrap();

        let mut decrypted = match state {
            DiaryState::None(no_diary) => match no_diary
                .create(
                    Some((key.clone(), hashing_data)),
                    MetaSecure::new(
                        Categories::default(),
                        Filters::new(CategorySelector::new(), CategoryFilterType::All, None),
                    ),
                    tokio::runtime::Handle::current(),
                )
                .await
            {
                Ok(v) => v,
                Err(e) => panic!("Failed to create diary {:?}", e.1),
            },
            DiaryState::Encrypted(_) => panic!("Diary shouldn't be encrypted"),
            DiaryState::Decrypted(_) => panic!("Diary shouldn't be decrypted"),
        };

        let id = RandomFileKey::generate(&*decrypted.storage_access, Some(key.to_owned()))
            .await
            .unwrap();

        decrypted.sections.insert(
            id,
            Manager::new_from_data(
                Arc::clone(&decrypted.storage_access),
                &id,
                Section::new(&decrypted.meta_secure.filters, &decrypted.sections),
                Some(key.to_owned()),
                Some(key),
            )
            .await
            .unwrap(),
        );

        decrypted
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn encrypted() {
        let fa = Arc::new(MemoryStorageAccess::default());

        {
            let mut state = DiaryState::get(Arc::clone(&fa), tokio::runtime::Handle::current())
                .await
                .unwrap();

            state
                .update(|state| async move {
                    Ok(match state {
                        DiaryState::None(no_diary) => DiaryState::Decrypted(
                            no_diary
                                .create(
                                    Some(
                                        encryption_data_from_password(
                                            &tokio::runtime::Handle::current(),
                                            Secret::new(b"bruh".to_vec()),
                                            1,
                                        )
                                        .await
                                        .unwrap(),
                                    ),
                                    MetaSecure::new(
                                        Categories::default(),
                                        Filters::new(
                                            CategorySelector::new(),
                                            CategoryFilterType::All,
                                            None,
                                        ),
                                    ),
                                    tokio::runtime::Handle::current(),
                                )
                                .await?,
                        ),
                        DiaryState::Encrypted(_) => panic!("Diary shouldn't be encrypted"),
                        DiaryState::Decrypted(_) => panic!("Diary shouldn't be decrypted"),
                    })
                })
                .await
                .unwrap();

            assert!(matches!(state, DiaryState::Decrypted(_)));
        }

        {
            let mut state = DiaryState::get(Arc::clone(&fa), tokio::runtime::Handle::current())
                .await
                .unwrap();

            decrypt(&mut state, Secret::new(b"bruh".to_vec())).await;

            let DiaryState::Decrypted(decrypted) = state else {
                panic!("Diary should be decrypted")
            };

            let hashing_data = decrypted.meta.encryption().unwrap();

            let key = Key::generate_key(
                &tokio::runtime::Handle::current(),
                Secret::new(b"bruhn't".to_vec()),
                hashing_data,
            )
            .await
            .unwrap();

            match decrypted
                .change_encryption(Some(key), Some(hashing_data))
                .await
            {
                Ok(_) => {}
                Err(e) => panic!("{:?}", e.1),
            }
        }
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn unencrypted() {
        let fa = Arc::new(MemoryStorageAccess::default());

        {
            let mut state = DiaryState::get(Arc::clone(&fa), tokio::runtime::Handle::current())
                .await
                .unwrap();

            state
                .update(|state| async move {
                    Ok(match state {
                        DiaryState::None(no_diary) => DiaryState::Decrypted(
                            no_diary
                                .create(
                                    None,
                                    MetaSecure::new(
                                        Categories::default(),
                                        Filters::new(
                                            CategorySelector::new(),
                                            CategoryFilterType::All,
                                            None,
                                        ),
                                    ),
                                    tokio::runtime::Handle::current(),
                                )
                                .await?,
                        ),
                        DiaryState::Encrypted(_) => panic!("Diary shouldn't be encrypted"),
                        DiaryState::Decrypted(_) => panic!("Diary shouldn't be decrypted"),
                    })
                })
                .await
                .unwrap();

            assert!(matches!(state, DiaryState::Decrypted(_)));
        }

        {
            let state = DiaryState::get(fa, tokio::runtime::Handle::current())
                .await
                .unwrap();

            assert!(matches!(state, DiaryState::Decrypted(_)));
        }
    }

    async fn decrypt(
        state: &mut DiaryState<MemoryStorageAccess<Reference<Path>, Vec<u8>>>,
        password: Secret<Vec<u8>>,
    ) {
        state
            .update(|state| async move {
                Ok(match state {
                    DiaryState::None(_) => panic!("Diary should exist"),
                    DiaryState::Encrypted(encrypted) => DiaryState::Decrypted({
                        let hashing_data = encrypted.meta.encryption().unwrap();

                        encrypted
                            .decrypt(
                                Key::generate_key(
                                    &tokio::runtime::Handle::current(),
                                    password,
                                    hashing_data,
                                )
                                .await
                                .unwrap(),
                            )
                            .await?
                    }),
                    DiaryState::Decrypted(_) => panic!("Diary shouldn't be decrypted"),
                })
            })
            .await
            .unwrap();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn encrypt_existing() {
        let fa = Arc::new(MemoryStorageAccess::default());

        let (key, hashing_data) = encryption_data_from_password(
            &tokio::runtime::Handle::current(),
            Secret::new(b"oof".to_vec()),
            1,
        )
        .await
        .unwrap();

        {
            let decrypted = test_diary(Arc::clone(&fa)).await;

            match decrypted
                .change_encryption(Some(key.clone()), Some(hashing_data))
                .await
            {
                Ok(_) => {}
                Err(e) => panic!("{:?}", e.1),
            };
        }

        {
            let mut state = DiaryState::get(Arc::clone(&fa), tokio::runtime::Handle::current())
                .await
                .unwrap();

            decrypt(&mut state, Secret::new(b"oof".to_vec())).await;

            let DiaryState::Decrypted(decrypted) = state else {
                panic!("Diary should be decrypted")
            };

            assert_eq!(decrypted.sections.len(), 1);
        }
    }
}
