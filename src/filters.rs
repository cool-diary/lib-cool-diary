use std::{path::Path, sync::Arc};

use electra::{Reference, StorageAccess};
use error_stack::{Report, Result};
use futures::{stream, StreamExt, TryStreamExt};

use crate::{Categories, CategorySelector, Key, LoadMediaError, Media};

use super::{Section, Time};

/// How categories should filter sections
#[derive(Clone, Copy)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub enum CategoryFilterType {
    /// A section must match any of the selected categories
    Any,
    /// A section must match all of the selected categories
    All,
    /// A section must match none of the selected categories
    Not,
}

/// The set of filters that are used to filter sections
#[derive(Clone)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Filters {
    pub(crate) selected_categories: CategorySelector,
    pub(crate) category_filter_type: CategoryFilterType,
    pub(crate) time: Option<Time>,
    pub(crate) text_search: String,
}

impl Filters {
    /// Create a new `Filters` instance
    pub const fn new(
        selected_categories: CategorySelector,
        category_filter_type: CategoryFilterType,
        time: Option<Time>,
    ) -> Filters {
        Filters {
            selected_categories,
            category_filter_type,
            time,
            text_search: String::new(),
        }
    }

    /// Get the category selector for the filters
    pub const fn category_selector(&self) -> &CategorySelector {
        &self.selected_categories
    }

    /// Get a mutable category selector for the filters
    pub fn category_selector_mut(&mut self) -> &mut CategorySelector {
        &mut self.selected_categories
    }

    /// Set the time filter
    pub fn set_time(&mut self, time: Option<Time>) {
        self.time = time;
    }

    /// Set the search term
    pub fn set_text_search(&mut self, text_search: &str) {
        self.text_search = text_search.to_lowercase();
    }

    /// Set the category filter type
    pub fn set_category_filter_type(&mut self, category_filter_type: CategoryFilterType) {
        self.category_filter_type = category_filter_type;
    }

    /// Check if a section matches the filters
    pub async fn section_matches_filters<
        SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
    >(
        &self,
        sa: Arc<SA>,
        key: Option<Key>,
        section: &Section<SA>,
        categories: &Categories,
    ) -> Result<bool, LoadMediaError> {
        Ok(self.matches_category_filter(
            section.categories().category_selector().to_vec(),
            categories,
        ) && self.matches_time_filter(section.categories().time())
            && (self.text_search.is_empty()
                || self
                    .matches_text_search(sa, key, section.title(), section.files())
                    .await?))
    }

    fn matches_category_filter(&self, mut category_ids: Vec<u32>, categories: &Categories) -> bool {
        category_ids = categories.add_parents(category_ids);

        let mut selected_categories_iter = self.selected_categories.iter();

        match self.category_filter_type {
            CategoryFilterType::Any => selected_categories_iter.any(|v| category_ids.contains(v)),
            CategoryFilterType::All => selected_categories_iter.all(|v| category_ids.contains(v)),
            CategoryFilterType::Not => selected_categories_iter.all(|v| !category_ids.contains(v)),
        }
    }

    fn matches_time_filter(&self, time: Time) -> bool {
        self.time
            .map_or(true, |time_filter| time_filter.overlaps(time))
    }

    async fn matches_text_search<
        SA: StorageAccess<FileKey = Reference<Path>, DataType = Vec<u8>> + ?Sized,
    >(
        &self,
        sa: Arc<SA>,
        key: Option<Key>,
        title: &str,
        files: &[Media<SA>],
    ) -> Result<bool, LoadMediaError> {
        Ok(title.to_lowercase().contains(&self.text_search)
            || stream::iter(files.iter())
                .then(|v| {
                    let sa = Arc::clone(&sa);
                    let key = key.to_owned();

                    async move {
                        match v {
                            Media::Text(text) => {
                                text.load(sa, key.to_owned(), key).await?;
                                Ok(text.latest().unwrap().to_lowercase())
                            }
                        }
                    }
                })
                .try_fold(false, |a, v| async move {
                    Ok::<_, Report<LoadMediaError>>(a || v.contains(&self.text_search))
                })
                .await?)
    }

    /// Get the time filter
    pub const fn time(&self) -> Option<Time> {
        self.time
    }

    /// Get the search term
    pub fn text_search(&self) -> &str {
        self.text_search.as_ref()
    }

    /// Get the category filter type
    pub const fn category_filter_type(&self) -> CategoryFilterType {
        self.category_filter_type
    }
}

#[cfg(test)]
mod test {
    use std::sync::Arc;

    use crate::{Categories, CategoryFilterType, CategorySelector, Filters, MediaEdits, Text};
    use electra::MemoryStorageAccess;
    use test_case::test_case;
    use CategoryFilterType::{All, Any, Not};

    #[test_case(Any, [0, 2], true  ; "Any filter, passing")]
    #[test_case(Any, [3, 2], false ; "Any filter, failing")]
    #[test_case(All, [0, 1], true  ; "All filter, passing")]
    #[test_case(All, [0, 2], false ; "All filter, failing")]
    #[test_case(Not, [2, 3], true  ; "Not filter, passing")]
    #[test_case(Not, [0, 2], false ; "Not filter, failing")]
    fn test_category_filter(
        category_filter_type: CategoryFilterType,
        selected_categories: impl Into<Vec<u32>>,
        expected_result: bool,
    ) {
        let filters = Filters {
            selected_categories: CategorySelector::new_with(selected_categories.into()),
            category_filter_type,
            time: None,
            text_search: String::new(),
        };

        let mut categories = Categories::default();
        categories.create_category("bruh".to_owned());
        categories.create_category("bruh".to_owned());
        categories.create_category("bruh".to_owned());
        categories.create_category("bruh".to_owned());

        assert_eq!(
            filters.matches_category_filter(vec![0, 1], &categories),
            expected_result
        );
    }

    #[test_case("The Etymology of Bruh", "brother", true ; "Title contains search")]
    #[test_case("The Etymology of Bruh", "Bruh is short for brother", true ; "Both contain search")]
    #[test_case("My current emotion", "bruhn't", true ; "Text contains search")]
    #[test_case("My current emotion", "poggers", false ; "Nothing contains search")]
    fn test_text_search(title: &str, text: &str, expected_result: bool) {
        tokio::runtime::Runtime::new().unwrap().block_on(async {
            let filters = Filters {
                selected_categories: CategorySelector::new(),
                category_filter_type: All,
                time: None,
                text_search: "bruh".to_string(),
            };

            let sa = Arc::new(MemoryStorageAccess::default());

            let text_file = MediaEdits::<Text, _>::new(
                Arc::clone(&sa),
                None,
                0,
                Text::from(text.to_owned()),
                None,
            )
            .await
            .unwrap();

            assert_eq!(
                filters
                    .matches_text_search(sa, None, title, &[crate::Media::Text(text_file)])
                    .await
                    .unwrap(),
                expected_result
            );
        });
    }

    #[test]
    fn category_filter_matches_parents() {
        let mut categories = Categories::default();

        let (category_1_id, _) = categories.create_category("A".to_string());

        let filters = Filters {
            selected_categories: CategorySelector::new_with(vec![category_1_id]),
            category_filter_type: All,
            time: None,
            text_search: String::new(),
        };

        let (category_2_id, category_2) = categories.create_category("B".to_string());

        category_2.set_parent(Some(category_1_id));

        assert!(filters.matches_category_filter(vec![category_2_id], &categories));
    }
}
